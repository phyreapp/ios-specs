#
#  Be sure to run `pod spec lint ApplePay.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://guides.cocoapods.org/syntax/podspec.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |spec|

  spec.name = "ApplePayProvisioning"
  spec.version = "1.2.0"
  spec.summary = "Apple Pay provisioning feature"

  spec.platform = :ios, "11.0"

  spec.homepage = "https://www.phyreapp.com"

  spec.license = "Code is MIT, then custom font licenses."

  spec.author = { "Phyre" => "hi@phyreapp.com" }
  spec.ios.deployment_target = '11.0'
  spec.ios.vendored_frameworks = 'ApplePayProvisioning.xcframework'
  spec.source = { :http => 'https://gitlab.com/phyreapp/ios-frameworks/-/raw/5be78cbbe8dc29ab93f9587bd9c9cce17db38733/ApplePayProvisioning/1.2.0/ApplePayProvisioning.zip'}
  
  spec.frameworks = 'UIKit', 'PassKit', 'Foundation'
  
  spec.dependency 'EasyPeasy'
  spec.dependency 'AlamofireObjectMapper', '~> 5.2.0'

  spec.swift_version = '4.2'
end
