#
#  Be sure to run `pod spec lint ApplePay.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://guides.cocoapods.org/syntax/podspec.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |spec|

  spec.name = "ApplePayProvisioning"
  spec.version = "3.0.1"
  spec.summary = "Apple Pay provisioning feature"

  spec.platform = :ios, "13.0"

  spec.homepage = "https://www.phyreapp.com"

  spec.license = "Code is MIT, then custom font licenses."

  spec.author = { "Phyre" => "hi@phyreapp.com" }
  spec.ios.deployment_target = '13.0'
  spec.source = { :http => 'https://gitlab.com/phyreapp/ios-frameworks/-/raw/218e514f566367ab93d699b047681ea1c94c22eb/ApplePayProvisioning/3.0.1/ApplePayProvisioning.zip'}
  
  spec.frameworks = 'UIKit', 'PassKit', 'Foundation'
  
  spec.dependency 'EasyPeasy', '1.10.0'
  spec.dependency 'AlamofireObjectMapper', '5.2.1'

  spec.vendored_frameworks = 'ApplePayProvisioning.xcframework'

  spec.swift_version = '5.0'
end
